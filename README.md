# BUNTURE(Bunny Adventure)
_Cake em all_
https://shafiraishlah51.itch.io/bunture

**99.9% players fails at the first try**

Bunture (Bunny Adventure) is a simple adventure game where the player take role as a bunny that wants to get the giant cake. The giant cake is somehow hidden in the last stage and the player But you need to be careful as bunny has only one chance to defeat  numerous challenge and traps to pass every level. So, beware of your steps as they will always have consequences. 

**Play and become the 0.01%.**

## Features
- The game has 5 levels and each level has increasing difficulties.
- You can collect coins that shattered accross the level and the coin will be displayed at the user interface. 
- Have in game music that encourage player

## How to play
- Up Arrow to Jump
- Left Arrow to Run to the left
- Right Arrow to Run to the right

## Install Instruction
- Download the zip and extract all the folders
- Run BUNTURE.exe
- "Voila" you can play the game!

## Screenshot

**Main Menu**
![Main Menu](https://i.imgur.com/n3KX4tT.jpg)

**Level 1**
![Level 1](https://i.imgur.com/KtXvIhU.jpg)

**Level 2**
![Level 2](https://i.imgur.com/t6pl2ec.jpg)

**Level 3**
![Level 3](https://i.imgur.com/rvcNMzm.jpg)

**Level 4**
![Level 4](https://i.imgur.com/TO2bual.jpg)

**Level 5**
![Level 5](https://i.imgur.com/4BxRVK4.jpg)

**Game Over**
![Game Over](https://i.imgur.com/t0g38XC.jpg)

**Win Screen**
![Win Screen](https://i.imgur.com/c01XoCa.jpg)

## Game's source gitlab 
https://gitlab.com/shafiraishlah51/1-week-game-jam.git

## Game's assets : 
- Player : made by myself
- https://bayat.itch.io/platform-game-assets?download
- https://free-game-assets.itch.io/free-parallax-2d-backgrounds
- https://hollowsphere.itch.io/2d-space-parallax-backgrounds
- https://ansimuz.itch.io/mountain-dusk-parallax-background
- https://raventale.itch.io/parallax-background
- https://free-game-assets.itch.io/free-horizontal-game-backgrounds
