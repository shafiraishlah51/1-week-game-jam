extends LinkButton

export(String) var scene_to_load

func _on_Return_To_Main_Menu_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
