extends Node2D

export (String) var sceneName = "Level 1"

func _ready():
	global.lives = 1
	var game_over = self.get_node("/root/Game Over Screen/Game Over").play()
	
func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		global.lives = 1
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
