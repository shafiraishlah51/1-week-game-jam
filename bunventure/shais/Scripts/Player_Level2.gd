extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()
var jumps = 0

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")
onready var bgm = self.get_node("/root/Level 2/BGM").play()

func get_input():
	var player_sprite = get_node("/root/Level 2/Player/Sprite")
	velocity.x = 0

	# Ground slam
	if Input.is_action_pressed('down') and not is_on_floor() and jumps >= 2:
		velocity.y -= jump_speed
		print("GROUND SLAM")

	# Moves to the right
	if Input.is_action_pressed('right'):
		velocity.x += speed
		player_sprite.set_flip_h(false)

	# Moves to the left
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		player_sprite.set_flip_h(true)

	if Input.is_action_just_pressed("up"):

		# Single jump
		if is_on_floor():
			print("MASUK BLOK IS ON FLOOR")
			velocity.y = jump_speed
			jumps += 1
			print("JUMPS:" + str(jumps))

		# Double jump
		elif not is_on_floor() and jumps == 1:
			print("MASUK BLOK SINGLE JUMP")
			velocity.y = jump_speed
			jumps += 1
			print("JUMPS:" + str(jumps))

		# Handle jump counter
		elif not is_on_floor() and jumps == 2:
			print("HAS DOUBLE JUMPED")
			jumps = 0
			print("JUMPS:" + str(jumps))

	# Handle bug
	if is_on_floor() and jumps >= 2:
		jumps = 0

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		animator.play("Jump")
	elif velocity.x != 0:
		animator.play("Walk")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	else:
		animator.play("Idle")

func _on_Stage_2_pressed():
	pass # Replace with function body.
