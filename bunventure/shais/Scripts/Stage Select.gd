extends Node2D

export (String) var sceneName = "Level 1"

func _ready():
	print(self.get_path())
	var bgm_path = "/root/Stage Select/BGM"
	var main_menu = self.get_node(bgm_path).play()

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))