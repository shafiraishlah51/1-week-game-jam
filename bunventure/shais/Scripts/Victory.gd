extends Node2D

export (String) var sceneName = "Level 1"

func _ready():
	global.lives = 1
	global.coins = 0
	var bgm = self.get_node("/root/Win Screen/Victory").play()